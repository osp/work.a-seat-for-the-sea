ABOUT

A Seat for the Sea is an artistic-research project coordinated by Greet Brauwers, in duo with Raf Custers. 

In 2022 we travel via 7 stations towards enhanced comprehension of the sea, its existence and threats. Actors of all kinds but with particular sea experience are invited to take part in this journey. ASftS arose from previous work, new social developments and our concerns, particularly in relation to deep-sea mining.

CONTACT
info@aseatforthesea.com


A Seat for the Sea #1
Gesticulated conference at Framer Framed, Amsterdam
10 Feb 2022
Can we collectively represent what is still unthinkable, namely “how does the sea raise its voice”?

The so-called Blue Economy serves as a background for this meeting : it conceives industrial and investment projects which are (to be) carried out in the sea. The next Big Thing is deep-sea mining in which the Low Lands with its dredging companies and shipbuilders play a significant role. 

Yet, in the setting of FramerFramed's exhibition 'Court for Intergenerational Climate Crimes', created by Radha D’Souza & Jonas Staal, it is more than appropriate to hear how artists with a strong bond with the sea relate to this new industrial niche. So, once again : How can we strengthen the voice of the sea? And, dilemma, can we talk in the name of the sea?

The event was also streamed :  https://www.youtube.com/watch?v=A2V4dtkZcgs


For this first public moment of ASftS, Greet Brauwers and Raf Custers invite the following artists to share their practices and their views :

MARIALENA MAROUDA works in the intersections between performance and sound art. She is the initiator of The Oceanographies Institute. 

ESTHER KOKMEIJER is artist, explorer, designer and photographer. Since 2013 she also works as an expedition photographer and polar guide, sailing to the Arctic during the Arctic summer and to Antarctica during the Antarctic summer. 

THEUN KARELSE studied fine arts in Amsterdam before joining FoAM, a trans disciplinary laboratory at the ‘interstices’ of art, science, nature and everyday life. His interests and experimental practice explore edges between art, environment, technology and archaeology. The Embassy of the Northsea invited him to be part of the fieldwork team for the future of the Delta.

STIJN DEMEULENAERE is a sound artist from Brussels. He makes installations, soundscapes and performances and does sound design for dance and theatre. He also does I also do a lot of field recording, which I then use in installations, compositions and live sets.

ANNA LUYTEN organises the exchanges as a conversator. She's a philosopher, does artistic research at the School of Arts, KASK Ghent and lectures in cultural criticism at the drama academy in Maastricht.


