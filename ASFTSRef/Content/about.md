About

Initiated by 
Greet Brauwers and Raf Custers
contact us at info[at]aseatforthesea.com

Financial support 
Framer Framed, development grant by the Flemish Government

Partners 
Framer Framed, Climaxi, Dr Fanny Wonu Veys (Nationaal Museum van Wereldculturen), Deep Sea Conservation Coalition, Theater aan Zee, kleinVerhaal, FMDO, O.666, MSc Marine and Lacustrine Science and Management (‘Oceans & Lakes’, VUB), Seas At Risk, Choux de Bxl

Website 
Open Source Publishing
