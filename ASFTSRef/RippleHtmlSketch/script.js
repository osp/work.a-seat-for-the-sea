//GUI for playing with the SVG turbulence filter 

var turb = document.querySelectorAll('#titleNoise feTurbulence')[0];
var dm = document.querySelectorAll('#titleNoise feDisplacementMap')[0];
var params = {
	baseFrequencyX: 0.0050,
	baseFrequencyY: 0.0050,
        scale: 40,
  turbOctaves: 2,
  dmOctaves: 2,
}
var gui = new dat.GUI();
gui.add(params, 'baseFrequencyX').min(0.0001).max(0.01).step(0.0001).onChange(update);
gui.add(params, 'baseFrequencyY').min(0.0001).max(0.01).step(0.0001).onChange(update);
gui.add(params, 'scale').min(0).max(100).step(1).onChange(update);
gui.add(params, 'turbOctaves').min(1).max(10).step(1).onChange(update);
gui.add(params, 'dmOctaves').min(1).max(10).step(1).onChange(update);

function update() {
	turb.setAttribute('baseFrequency', params.baseFrequencyX + ' ' + params.baseFrequencyY);
        turb.setAttribute('numOctaves', params.turbOctaves);
        dm.setAttribute('scale', params.scale);
        dm.setAttribute('numOctaves', params.dmOctaves);
}

var text = new Blotter.Text("A Seat for the Sea - Blotter.js", {
        family : "serif",
        size : 32,
        fill : "#171717"
      });

var material = new Blotter.LiquidDistortMaterial();
      
material.uniforms.uSpeed.value = 0.1

material.uniforms.uVolatility.value = 0.1

var blotter = new Blotter(material, { texts : text });

var scope = blotter.forText(text);

scope.appendTo(document.body);

