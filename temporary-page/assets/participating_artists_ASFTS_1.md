## Participating artists ASFTS

---

**Marialena Marouda** (GR/DE/BE) works in the intersections between performance, sound art and oral poetry. She studied philosophy and visual arts at Columbia University in New York, USA and continued her studies at the Institute for Applied Theatre Studies at the University of Giessen, Germany. In May 2018 she initiated The Oceanographies Institute (TOI), as part of her research at the Advanced Performance and Scenography Studies Program (a.pass) in Brussels.

TOI focuses essentially on the relation between two bodies of water: the human body and the world ocean(s). It gives particular attention to affectual and sensual encounters between the two bodies. The Institute therefore explores the relations of hands to mud, ears to the breaking waves, feet to the feeling of sinking, rather than the ocean “in itself”, as if devoid of the human presence. It collects, analyzes and reenacts people's personal stories about their encounters with the ocean. In 2019, composer Charlie Usher and performance maker Elpida Orfanidou joined TOI, letting their practices of song-making and voicing flow into the institute‘s work.

[www.poetryexercises.de](www.poetryexercises.de)

---

**Theun Karelse** (Nl) studied fine-arts at the Sandberg Institute in Amsterdam before joining FoAM, a transdisciplinary laboratory at the interstices of art, science, nature and everyday life. His interests and experimental practice explore edges between art, environment, technology and archaeology. Lately he has been creating research programmes that consist of fieldwork as a means of critical reflection. For this diverse teams are established to address specific topics in specific locations by in-situ prototyping, experimentation and direct
perception.

I’m interested in environmental literacy, in humans and other organisms. My research is both text based and field based. So besides keeping up with cutting edge thought on environment, I engage with people at the forefront of the practice. Not just a few weeks, but many many years of working with farmers, landscapers, ecosystem regenerators, indigenous peoples, ecologists, biologists, and various others that defy labeling. To not talk about nature, but be in conversation with nature.

I’m from Zeeland. Grown up in Borssele, so was my father, my grandfather, etc. Less than 100m from the family home is a 9m flood mount. The previous village of Monster (and two others close by) was wiped away in the floods November of 1530. Borssele was rebuild there in 1616 as a planned village.

The Embassy of the Northsea invited me to be part of the fieldwork team for the future of the Delta, with Darko Lagunas (anthropology) and Maarten Kleinhans (geoscience). This team has been engaged with the first stage of research at the Embassy, the stage of listening.
How can we listen to other beings? What can be heard? We’ve done interviews with people who have a special, longstanding, or profound connection to non-human actors in the region.
Based on this I’ve conducted some physical experiments. Not be be like a non-human but to try and enter its world. I’m working on the basis that non-humans for the most part of human history have been vital and highly valued guides and messengers. Far beyond notions of ‘indicator-species’ as we now know in ecological science. Seeing our current predicament, in a rapidly changing world, the vast array of intelligences around us may be more important than ever in informing us and guiding us.

[http://theunkarelse.net/fieldwork.html](http://theunkarelse.net/fieldwork.html)

---

**Esther Kokmeijer**, born in Dokkum, The Netherlands (1977), is an artist, explorer, designer and photographer, currently residing in Rotterdam and working around the globe.
In her work as an artist, she mainly focuses on the ‘Global Commons’. A term typically used to describe international, supranational, and global resource domains. Global commons include the earth’s shared natural resources, such as the high oceans, the atmosphere and outer space and the Antarctic in particular.

Recurring motifs and questions in her work are; How do myths and sagas express and shape mankind’s relationship with nature? How do we relate to and work with the elements and our environment? How do they exert influence on us? What are humanity’s effects on geological and biological processes? What can we learn from indigenous knowledge systems, can it deepen or induce a more sustainable relationship to our natural environment, and will it bring more solidarity with the landscape?

Seasonally she works as an expedition photographer and polar guide, in the Arctic and Antarctic. She is the founder of ANTARKTIKOS, a magazine that is solely dedicated to Antarctica ([antarktikos.com](antarktikos.com)), co founder of Cosmic Water Foundation ([cosmicwaterfoundation.com](cosmicwaterfoundation.com)) and part of the art collective If Paradise Is Half As Nice ([ipihan.com](Ipihan.com)).

[http://www.estherkokmeijer.nl/](http://www.estherkokmeijer.nl/)

---

**Stijn Demeulenaere** is a sound artist, searching musician, and field recordist. He holds degrees in sociology, cultural studies and studied radio at the RITCS, Royal Institute for Theatre, Cinema and Sound.

Stijn is attracted to sound because of its directness, its malleability, and its mystery. In sound he tries to unravel social structures, personal history and the unconscious imagination of people.

He is currently working around the themes of the phenomenology of listening and the personal experience of sound. Stijn researches how people give meaning to sound, and the relationships between identity, sound and listening. Apart from creating his own work, Stijn also creates the sound design for other makers. Among others, he created soundtracks for choreographers, theatre makers and he works with directors and video artists like Jan Locus and Visual Kitchen. Recently he also started collaborating with several musicians.

Stijn has been artist in residence at among others, Overtoon (Brussels, BE), Cimatics (Brussels, BE), and Imagine2020 (UK). He is currently artist in residence at the Kunstenwerkplaats VZW in Brussels (BE). His work won prizes at the 2019 Engine Room International Sound Art competition (UK), the 2014 soundscape composition prize of Musica (BE) and won the Grand Prix at the 2020 Split Videoart Festival. His work was also nominated for, among others, the 2016 Discovery Award at LOOP Barcelona (ES), and the 2016 European Sound Art Award (DE). In 2017, he released his first album Latitudes – September 2016 on the Silken Tofu Label. Stijn lives and works in Brussels (BE).

[www.stijndemeulenaere.be](www.stijndemeulenaere.be)

---

## Initiators

---

**Raf Custers** (BE) lives and works in Brussels. He is a writer, doc-maker, singer-performer, historian (Master Contemporary History, KU.Leuven, specialisation interwar period/Latin America/semiotics) and freelance journalist. He has been part of recent public interventions ‘ChoeurFew’, ‘BROcean-Zeemonster’ and Bru(i)tal and in the musical performance ‘What happened to the dead fish’ (2021, kfda), ‘Melancholy at the 5 blocks’ (BBEK/Kaaitheater) and ‘Oslo’ (2018, Kaaitheater). He makes visual work in the graphic arts studio of the academy of Anderlecht. For ‘Melancholy at the 5 Blocks’ (2020) he did the dramaturgy.

A lot of visual work is created in cooperation with Greet Brauwers such as the transmedia project ‘Re-Pulse’ (since 2019) and the award-winning documentary ‘Lithium, curse or blessing for Bolivia’ (2011). Together they undertook a study tour in 2013-2014 through South America for fieldwork on extractivism, the city and civil resistance, from which a book and numerous video documents. The documentary ‘Cri d'alarme du Kivu’ was awarded the Silver Sign in Rome in 2001. Raf Custers published books on stowaways, media activism, the multinationals of global mining and extractivism. He has worked for radio, television, newspapers, web media and magazines (Mediamatic, Andere Cinema, Africa in the Picture...).

---

**Greet Brauwers** (BE) worked as a journalist (mail topics: art, diversity and feminism) after a master's degree in audiovisual arts at the KASK. She is the author of several documentaries including 'Bruxelles-Kinshasa', 'Do You Copy Me?', 'Torture in Baraka', 'Marrying across the border' and 'Everyone migrant'. The documentary 'Lithium, curse or blessing for Bolivia' was awarded the press prize of the Council for Sustainable Development.
Besides journalistic work, she also created participatory socio-artistic projects. In 2005, she joined the alternative media platform Indymedia.be.

Since 2017, she has been working at the intersection of documentary, anthropology and visual arts. In collaboration with SoundImageCulture (SIC), she realised the film 'Traces Mathieu Corman, A Platform for Phantoms of the Past' (part of a performance at Beursschouwburg, Chambres d'O and TAZ). Now she’s developing the transmedia project 'Repulse' in colaboration with Raf Custers.

[http://www.soulpress.be](http://www.soulpress.be)
